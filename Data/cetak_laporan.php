<?php
require('assets/lib/fpdf.php');
class PDF extends FPDF
{
    function Header()
    {
        $this->SetFont('Times','B',30);
        $this->Cell(30,10,'KOST-DESTYA');

        $this->Ln(10);
        $this->SetFont('Arial','i',10);
        $this->cell(30,10,'JL Ketintang Timur PTT III No 32, Surabaya');


        $this->Ln(5);
        $this->SetFont('Arial','i',10);
        $this->cell(30,10,'Telp/Fax : 081-273-338-544');


        $this->Ln(5);
        $this->SetFont('Arial','i',10);
        $this->cell(30,10,'Data Laporan Tanggal : '.$_POST['tgl_laporan'].'');

        $this->Ln(5);
        $this->SetFont('Arial','i',10);
        $this->cell(30,10,'Jenis : '.$_POST['jenis_laporan'].'');

        $this->cell(130);
        $this->SetFont('Arial','',10);
        $this->cell(30,10,'Surabaya, '.date("d-m-Y").'');

        $this->Line(10,45,205,45);
    }
    function data_barang(){
        $link= mysqli_connect("localhost","root","");
        mysqli_select_db($link, "imk");
        $tanggal=$_POST['tgl_laporan'];
        if ($_POST['jenis_laporan']=="perhari") {
            $split1=explode('-',$tanggal);
            $tanggal=$split1[2]."-".$split1[1]."-".$split1[0];
            $query=mysqli_query($link, "select transaksi.id_transaksi,transaksi.tgl_transaksi,transaksi.no_invoice,transaksi.total_bayar,transaksi.nama_pembeli,transaksi.bulan,user.username from transaksi inner join user on transaksi.kode_kasir=user.id where transaksi.tgl_transaksi like '%$tanggal%' order by transaksi.id_transaksi desc");
        }else{
            $split1=explode('-',$tanggal);
            $tanggal=$split1[1]."-".$split1[0];
            $query=mysqli_query($link, "select transaksi.id_transaksi,transaksi.tgl_transaksi,transaksi.no_invoice,transaksi.total_bayar,transaksi.nama_pembeli,transaksi.bulan,user.username from transaksi inner join user on transaksi.kode_kasir=user.id where transaksi.tgl_transaksi like '%$tanggal%' order by transaksi.id_transaksi desc");
        }
        while ($r=  mysqli_fetch_array($query))
                {
                    $hasil[]=$r;
                }
                return $hasil;
                
    }
    function set_table($data){
        $this->SetFont('Arial','B',9);
        $this->Cell(7,7,"No",1);
        $this->Cell(32,7,"No Invoice",1);
        $this->Cell(40,7,"Petugas",1);
        $this->Cell(30,7,"Nama Penghuni",1);
        $this->Cell(28,7,"Bulan",1);
        $this->Cell(33,7,"Tanggal Transaksi",1);
        $this->Cell(25,7,"Total Bayar",1);
        $this->Ln();

        $this->SetFont('Arial','',9);
        $no=1;
        foreach($data as $row)
        {
            $this->Cell(7,7,$no++,1);
            $this->Cell(32,7,$row['no_invoice'],1);
            $this->Cell(40,7,$row['username'],1);
            $this->Cell(30,7,$row['nama_pembeli'],1);
            $this->Cell(28,7,$row['bulan'],1);
            $this->Cell(33,7,date("d-m-Y h:i:s",strtotime($row['tgl_transaksi'])),1);
            $this->Cell(25,7,"Rp. ".number_format($row['total_bayar']),1);
            $this->Ln();
        }
    }
}

$pdf = new PDF();
$pdf->SetTitle('Cetak Data Barang');

$data = $pdf->data_barang();

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Ln(20);
$pdf->set_table($data);
$pdf->Output('','Kost-Destya/Laporan/'.date("d-m-Y").'.pdf');
?>