<script type="text/javascript">
	document.title="Transaksi Baru";
	document.getElementById('transaksi').classList.add('active');
</script>
<script type="text/javascript">
		$(document).ready(function(){
			if ($.trim($('#contenth').text())=="") {
				$('#prosestran').attr("disabled","disabled");
				$('#prosestran').attr("title","tambahkan barang terlebih dahulu");
				$('#prosestran').css("background","#ccc");
				$('#prosestran').css("cursor","not-allowed");
			}
		})

</script>
<div class="content">
	<div class="padding">
		<div class="bgwhite">
			<div class="padding">
				<h3 class="jdl">Entry  Transaksi Baru</h3>
				<form class="form-input" method="post" action="handler.php?action=tambah_tempo" style="padding-top: 30px;">
					<label>Pilih Kamar  : </label>
					<select style="width: 372px;cursor: pointer;" required="required" name="id_barang">
						<?php
						$data=$root->con->query("select * from barang");
						while ($f=$data->fetch_assoc()) {
							echo "<option value='$f[id_barang]'>$f[nama_barang]  | Harga : ".number_format($f['harga_jual']).")</option>";
						}
						?>
					</select>
					<input required="required" type="hidden" value="1" name="jumlah">
					<input type="hidden" name="trx" value="<?php echo date("d")."/AF/".$_SESSION['id']."/".date("y") ?>">
					<button class="btnblue" type="submit"><i class="fa fa-save"></i> Simpan</button>
				</form>
				
			</div>
		</div>
		<br>
		<div class="bgwhite">
			<div class="padding">
				<h3 class="jdl">Data transaksi</h3>
				<table class="datatable" style="width: 100%;">
				<thead>
				<tr>
					<th width="35px">NO</th>
					<th>ID KAMAR</th>
					<th>DETAIL NO KOST</th>
					<th>TOTAL HARGA</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody id="contenth">
				<?php
				$trx=date("d")."/AF/".$_SESSION['id']."/".date("y");
				$data=$root->con->query("select barang.nama_barang,tempo.id_subtransaksi,tempo.id_barang,tempo.jumlah_beli,tempo.total_harga from tempo inner join barang on barang.id_barang=tempo.id_barang where trx='$trx'");
				$getsum=$root->con->query("select sum(total_harga) as grand_total from tempo where trx='$trx'");
				$getsum1=$getsum->fetch_assoc();
				$no=1;
				while ($f=$data->fetch_assoc()) {
					?><tr>
					<td><?= $no++ ?></td>
						<td><?= $f['id_barang'] ?></td>
						<td><?= $f['nama_barang'] ?></td>
						<td>Rp. <?= number_format($f['total_harga']) ?></td>
						<td><a href="handler.php?action=hapus_tempo&id_tempo=<?= $f['id_subtransaksi'] ?>&id_barang=<?= $f['id_barang'] ?>&jumbel=<?= $f['jumlah_beli'] ?>" class="btn redtbl"><span class="btn-hapus-tooltip">Cancel</span><i class="fa fa-close"></i></a></td>
						</tr>
					<?php
				}
				?>
			</tbody>
				
				<tr>
					<?php if ($getsum1['grand_total']>0) { ?>
					<td colspan="3"></td><td>Grand Total :</td>
					<td> Rp. <?= number_format($getsum1['grand_total']) ?></td>
					<td></td>
					<?php }else{ ?>
					<td colspan="6">Data masih kosong</td>
					<?php } ?>
				</tr>
				
			</table>
			<br>
			<form class="form-input" action="handler.php?action=selesai_transaksi" method="post">
					<label>Nama Penghuni Kost :</label>
					<input required="required" type="text" name="nama_pembeli">
					<label>Bulan Pembayaran :</label>
				<select name="bulan" required>
				<option value="" disabled selected>---------------Pilih Bulan Pembayaran---------------</option>
				<option value="[01] JANUARI">Januari</option>
				<option value="[02] FEBRUARI">Februari</option>
				<option value="[03] MARET">Maret</option>
				<option value="[04] APRIL">April</option>
				<option value="[05] MEI">Mei</option>
				<option value="[06] JUNI">Juni</option>
				<option value="[07] JULI">Juli</option>
				<option value="[08] AGUSTUS">Agustus</option>
				<option value="[09] SEPTEMBER">September</option>
				<option value="[10] OKTOBER">Oktober</option>
				<option value="[11] NOVEMBER">November</option>
				<option value="[12] DESEMBER">Desember</option>
			</select>
					<input type="hidden" name="total_bayar" value="<?= $getsum1['grand_total'] ?>">
					<button class="btnblue" id="prosestran" type="submit"><i class="fa fa-save"></i> Proses Transaksi</button>
			</form>

			</div>
		</div>


	</div>
</div>

<?php
include "foot.php";
?>
